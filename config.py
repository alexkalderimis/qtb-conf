c.editor.command = ['neovide', '--nofork', '{}', '--', '-s', '/home/alex/.config/nvim/qute-editor.vim']

# with config.pattern('*://gitlab.com') as p:
c.content.user_stylesheets = ['/home/alex/.config/qutebrowser/css/base.css', '/home/alex/.config/qutebrowser/css/gitlab.css']
c.content.headers.user_agent = 'Default: Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'

c.auto_save.session = True

c.aliases['tabopen'] = 'open -t'
c.aliases['to'] = 'open -t'
c.aliases['e'] = 'open'
c.aliases['tc'] = 'config-cycle tabs.position top right'

c.url.searchengines['hoogle'] = 'https://hoogle.haskell.org/?hoogle={}&scope=set%3Astackage'
c.url.searchengines['gg'] = 'https://google.com/search?q={}'
c.url.searchengines['hackage'] = 'https://hackage.haskell.org/packages/search?terms={}'
c.url.searchengines['wiktionary'] = 'https://en.wiktionary.org/wiki/{}'
c.url.searchengines['glmr'] = 'https://gitlab.com/gitlab-org/gitlab/-/merge_requests/{}'

c.tabs.padding['bottom'] = 2
c.tabs.padding['top'] = 2
c.tabs.position = 'right'

config.load_autoconfig()
